package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"revolut-json-restapi/managers"
	"revolut-json-restapi/models"
	"time"

	"github.com/gorilla/mux"
)

var timeFormat = "2006-1-2"

func GetUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	vars := mux.Vars(r)
	userItem := managers.GetUser(vars["username"])
	if len(userItem.Username) == 0 {
		w.WriteHeader(404)
		json.NewEncoder(w).Encode(models.Result{
			Message: fmt.Sprintf("Not found! User with username %s does NOT exist in our system!", vars["username"]),
		})
		return
	}
	now := time.Now()
	userBirthday, _ := time.Parse(timeFormat, userItem.Birthday)
	yearsToAdd := now.Year() - userBirthday.Year()
	if (now.Month() > userBirthday.Month()) ||
		(now.Month() == userBirthday.Month() && now.Day() > userBirthday.Day()) {
		yearsToAdd += 1
	}
	userBirthday = userBirthday.AddDate(yearsToAdd, 0, 0)
	days := int(time.Until(userBirthday).Hours() / 24)
	hours := (int(time.Until(userBirthday).Hours()) % 24)
	if days == 0 && time.Until(userBirthday) <= 0 {
		json.NewEncoder(w).Encode(models.Result{
			Message: fmt.Sprintf("Hello, %s! Happy birthday!", userItem.Username),
		})
	} else {
		json.NewEncoder(w).Encode(models.Result{
			Message: fmt.Sprintf("Hello %s! Your birthday is in %d day{s) and %d hour(s).", userItem.Username, days, hours),
		})
	}
}

func SetUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	vars := mux.Vars(r)
	username := vars["username"]
	isAlpha := regexp.MustCompile(`^[A-Za-z]+$`).MatchString
	birthday := vars["birthday"]
	userBirthday, _ := time.Parse(timeFormat, birthday)
	if time.Since(userBirthday) < 0 {
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(models.Result{
			Message: fmt.Sprintf("Bad request! Birthday should be in the past!"),
		})
	} else if !isAlpha(username) {
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(models.Result{
			Message: fmt.Sprintf("Bad request! %q is not valid username, please use only letters!", username),
		})
	} else {
		w.WriteHeader(204)
		managers.SetUser(username, birthday)
	}
}
