FROM golang:1.12.5-alpine3.9
LABEL Author="Nikola.Zelenkov <z@3zinventions.com>"
ENV APPNAME=revolut-json-restapi
ENV GOBIN=/go/bin
ENV GOPATH=/go/src/bitbucket.org/3ZInventions
ENV CGO_ENABLED=0
WORKDIR ${GOPATH}/src/${APPNAME}
COPY . /tmp/${APPNAME}
RUN apk update && apk add git && \
cd /tmp/${APPNAME} && tar -cO --exclude=.dockerignore . | tar -xv -C ${GOPATH}/src/$APPNAME && \
cd ${GOPATH}/src/${APPNAME} && mkdir -p /data/store && \
go get -d -v && go build -v && go install -v && go test -v ./... && \
rm -rf /var/cache/apk/* && rm -rf /tmp/${APPNAME}
VOLUME store /data/store
CMD ["revolut-json-restapi"]
