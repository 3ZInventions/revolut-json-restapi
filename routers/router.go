package routers

import (
	"net/http"
	"revolut-json-restapi/handlers"

	"github.com/gorilla/mux"
)

type Route struct {
	Name        string
	Methods     []string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

var routes []Route

func init() {
	routes = append(routes, Route{
		Name:        "SetUser",
		Methods:     []string{"PUT"},
		Pattern:     "/hello/{username}/{birthday}",
		HandlerFunc: handlers.SetUser,
	})

	routes = append(routes, Route{
		Name:        "GetUser",
		Methods:     []string{"GET"},
		Pattern:     "/hello/{username}",
		HandlerFunc: handlers.GetUser,
	})
}

func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)

	for _, route := range routes {
		var handler http.Handler
		handler = route.HandlerFunc

		router.
			Methods(route.Methods...).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	return router
}
