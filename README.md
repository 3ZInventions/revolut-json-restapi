# revolut-json-restapi

Wiki:
https://bitbucket.org/3ZInventions/revolut-json-restapi/wiki/Home

Diagram of solution:
https://bitbucket.org/3ZInventions/revolut-json-restapi/wiki/Revolut%20SRE%20Home%20Challenge

```
docker build -t rev/restapi:latest .
docker tag rev/restapi:latest 282436823485.dkr.ecr.eu-we-2.amazonaws.com/rev/restapi:latest
eval $(aws ecr get-login --no-include-email --region eu-west-2)
docker push 282436823485.dkr.ecr.eu-west-2.amazonaws.com/rev/restapi:latest

docker run --rm -ti -v $PWD/store:/data/store -p 33333:33333 rev/restapi

ecs-cli configure --cluster rev-cluster --region eu-west-2 --default-launch-type FARGATE --config-name revolut
ecs-cli configure profile --access-key $AWS_ACCESS_KEY_ID --secret-key $AWS_SECRET_ACCESS_KEY --profile-name revolut-profile
ecs-cli compose --project-name rev-restapi service up --ecs-profile revolut-profile --cluster-config revolut
```