package main

import (
	"log"
	"net/http"
	"revolut-json-restapi/routers"
)

func main() {
	router := routers.NewRouter()
	log.Fatal(http.ListenAndServe(":33333", router))
}
