package managers

import (
	"revolut-json-restapi/models"

	"github.com/rapidloop/skv"
)

var store, _ = skv.Open("/data/store/users.db")

func GetUser(username string) models.User {
	birthday := ""
	err := store.Get(username, &birthday)
	if err == nil {
		return models.User{Username: username, Birthday: birthday}
	}
	return models.User{}
}

func SetUser(username string, birthday string) {
	store.Put(username, birthday)
}
