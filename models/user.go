package models

type User struct {
	Username string
	Birthday string
}

type Result struct {
	Message string
}
